﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CashReg
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            PutButtonsBack();
        }
        void PutButtonsBack()
        {
             if(button1.Enabled == true)  this.button1.Location = new System.Drawing.Point(22, 336);
            if (button2.Enabled == true) this.button2.Location = new System.Drawing.Point(22, 554);
            if (button4.Enabled == true) this.button4.Location = new System.Drawing.Point(22, 445);
            if (button5.Enabled == true) this.button5.Location = new System.Drawing.Point(22, 663);

           

        }
        bool IsMouseDown;
        bool IsInBarcode;
        double  ScandPrice;
        Button CurrentProduct; 
        private void button1_MouseDown(object sender, MouseEventArgs e)
        {
            IsMouseDown = true;
        }

        private void button1_MouseUp(object sender, MouseEventArgs e)
        {
            IsMouseDown = false ;
            if(IsInBarcode)
            {
                textBox1.Text =ScandPrice .ToString();
                IsMouseDown = false;
                PutButtonsBack();
                IsInBarcode = false;
                textBox3.Text = "";
            }
            PutButtonsBack();
        }
        
        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            if (IsMouseDown)
            {
                button.Location = new Point((Cursor.Position.X - this.Location.X) -(button.Width / 2)
                    , (Cursor.Position.Y  - this.Location.Y) - (button.Height  / 2) - 25);
                if((Cursor.Position.X - this.Location.X) > pictureBox1 .Location.X && (Cursor.Position.X - this.Location.X) < pictureBox1.Location.X + pictureBox1.Width &&
                    (Cursor.Position.Y- this.Location.Y) > pictureBox1.Location.Y && (Cursor.Position.Y - this.Location.Y) < pictureBox1.Location.Y+ pictureBox1.Height )
                {
                    IsInBarcode = true;
                    ScandPrice = Convert.ToDouble(button.Tag);
                    CurrentProduct = button;
                }
                else
                {
                    IsInBarcode = false;
                    CurrentProduct = null; 
                }
                 
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;
            double  Pay = Convert.ToDouble(button.Text.Replace("$",""));
            
        }

        private void button14_Click(object sender, EventArgs e)
        {
            


            if (button14.Text == "Open")
            {
                button3.Visible = textBox4.Visible =
           button6.Visible = textBox5.Visible =
           button7.Visible = textBox6.Visible =
           button8.Visible = textBox7.Visible =
           button9.Visible = textBox8.Visible =
           button10.Visible = textBox9.Visible =
           button11.Visible = textBox10.Visible =
           button12.Visible = textBox11.Visible =
              button13.Visible = textBox12.Visible  = true;
                button14.Text = "Close";
            }
            else
            {
                button3.Visible = textBox4.Visible =
         button6.Visible = textBox5.Visible =
         button7.Visible = textBox6.Visible =
         button8.Visible = textBox7.Visible =
         button9.Visible = textBox8.Visible =
         button10.Visible = textBox9.Visible =
         button11.Visible = textBox10.Visible =
         button12.Visible = textBox11.Visible =
         button13.Visible = textBox12.Visible = false ;
                button14.Text = "Open";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "") return; 
            Double Price = Convert.ToDouble(textBox1.Text); 
            button16.Enabled = (Price < 50);
            button15.Enabled = (Price < 100);
            button17.Enabled = (Price < 200);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") return;

            Button button = sender as Button;
            Double Price = Convert.ToDouble(textBox1.Text); 
            double Pay = Convert.ToDouble(button.Text.Replace("$", ""));
            textBox3.Text = (Pay - Price).ToString();


        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            button18.Enabled = (textBox3.Text == "0");

        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked) return;
            CurrentProduct.Location=new Point(label5.Location.X, CurrentProduct.Location.Y  );
            CurrentProduct.Enabled = false;
            button18.Enabled = false; 
        }


        bool IsMonyMouseDown;
        bool IsInPayBox;
        double PayBack;
        TextBox PayBill;
        private void button3_MouseDown(object sender, MouseEventArgs e)
        {
            IsMonyMouseDown = true;
        }

        private void button3_MouseUp(object sender, MouseEventArgs e)
        {

            IsMonyMouseDown = false;
            
            if (IsInPayBox)
            {
                if (!(textBox3.Text == "" || (Convert.ToDouble(textBox3.Text) < PayBack))) {



                IsMonyMouseDown = false;
                textBox3.Text = (Convert.ToDouble(textBox3.Text) - PayBack).ToString();
                PayBill.Text = (Convert.ToDouble(PayBill.Text) - 1).ToString();
                IsInPayBox = false;
                
                }
            }
            this.button13.Location = new System.Drawing.Point(517, 566);
            this.button12.Location = new System.Drawing.Point(392, 566);
            this.button10.Location = new System.Drawing.Point(516, 413);
            this.button9.Location = new System.Drawing.Point(391, 413);
            this.button11.Location = new System.Drawing.Point(267, 566);
            this.button7.Location = new System.Drawing.Point(517, 249);
            this.button8.Location = new System.Drawing.Point(266, 413);
            this.button6.Location = new System.Drawing.Point(392, 249);
            this.button3.Location = new System.Drawing.Point(267, 249);
        }

        private void button3_MouseMove(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            if (IsMonyMouseDown)
            {
                button.Location = new Point((Cursor.Position.X - this.Location.X) - (button.Width / 2)
                    , (Cursor.Position.Y - this.Location.Y) - (button.Height / 2) - 25);
                if ((Cursor.Position.X - this.Location.X) > textBox3.Location.X -25 && (Cursor.Position.X - this.Location.X) < textBox3.Location.X + textBox3.Width + 25 &&
                    (Cursor.Position.Y - this.Location.Y) > textBox3.Location.Y - 25 && (Cursor.Position.Y - this.Location.Y) < textBox3.Location.Y + textBox3.Height + 25)
                {
                 
                    double Pay = Convert.ToDouble(button.Text.Replace("$", "")); 
                    IsInPayBox = true;
                    PayBack = Pay;
                    switch (Pay) 
                    {
                        case 0.5:
                            PayBill = textBox10;
                            break;
                        case 1:
                            PayBill = textBox11;
                            break;
                        case 2:
                            PayBill = textBox12;
                            break;
                        case 5:
                            PayBill = textBox7;
                            break;
                        case 10:
                            PayBill = textBox8;
                            break;
                        case 20:
                            PayBill = textBox9;
                            break;
                        case 50:
                            PayBill = textBox4;
                            break;
                        case 100:
                            PayBill = textBox5;
                            break;
                        case 200:
                            PayBill = textBox6;
                            break;

                    }

                }
                else
                {
                    IsInPayBox = false;
                    PayBill = null; 
                }

            }
        }
    }
}
